
//add handler for civimap
Drupal.gmap.addHandler('civimap', function(elem) {
  //get this gmap object
  var obj = this;
  
  //initialize the clientsidexml array
  if (!Drupal.gmap.clientsidexml) { Drupal.gmap.clientsidexml = {}; }
  
  //create function for when the map initializes
  obj.bind('init', function() {
    //get map id for reference  
    var mapid = obj.map.getContainer().id;
  
    //get input elements and determine change action
    $(elem).find('input').change(function() {
    
      if (this.checked) {
        //add new clientsidexml overlay
        Drupal.gmap.clientsidexml[mapid] = new EGeoXml("Drupal.gmap.clientsidexml['"+mapid+"']", obj.map, this.value);
        //parse map to display new overlay
        Drupal.gmap.clientsidexml[mapid].parse();
      } else {
        alert('Unchecked, but now what?');
        //TODO Remove markers.
        Drupal.gmap.clientsidexml[mapid].map.removeOverlay();
      }
      
      
    });
    
  });
  
});


    
