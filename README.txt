

Description
-------------------
This module aims to integrate CiviCRM data and the GMap Module.


Dependencies
-------------------
* CiviCRM
* GMap Module
* GMap Overlay Module which is part of the Gmap Addons Module
** Ensure that the third-party clientsidekml (egeoxml.js) is working
** http://econym.googlepages.com/egeoxml.htm


Functionality
-------------------
Functionality and features are still being determined.  We welcome your feedback on what you want to see with this module at http://drupal.org/node/add/project_issue/civimap/feature.


Access
-------------------
* Drupal Role Access
* CiviCRM Access